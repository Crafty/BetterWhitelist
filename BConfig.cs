﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
namespace BetterWhitelist
{
    public class BConfig
    {
        public bool Disabled { get; set; }
        public List<string> WhitePlayers = new List<string>();
        public static BConfig Load(string path) {
            if (File.Exists(path))
            {
                return JsonConvert.DeserializeObject<BConfig>(File.ReadAllText(path));
            }
            else {
                return new BConfig() { Disabled = false };
            }
        }
    }
}
